// Import nextjs module
import Image from 'next/image'
import { useRouter } from 'next/router'

// Import data
import kategori from '../data/kategori'
import hotels from '../data/populer-hotel'

export default function Boarding() {
	const router = useRouter()

	return (
		<>
			<section id="header" className="flex justify-between">
				<div>
					<h5 className="text-sm opacity-60">Hi Kautsar!</h5>
					<h2 className="text-lg font-medium">Mari buat rencan liburan!</h2>
				</div>
				<div className="flex items-center justify-center bg-white p-3 shadow-lg rounded-full relative min-w-[50px] min-h-[50px] cursor-pointer">
					<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
						<path d="M19 13.586V10c0-3.217-2.185-5.927-5.145-6.742C13.562 2.52 12.846 2 12 2s-1.562.52-1.855 1.258C7.185 4.074 5 6.783 5 10v3.586l-1.707 1.707A.996.996 0 0 0 3 16v2a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1v-2a.996.996 0 0 0-.293-.707L19 13.586zM19 17H5v-.586l1.707-1.707A.996.996 0 0 0 7 14v-4c0-2.757 2.243-5 5-5s5 2.243 5 5v4c0 .266.105.52.293.707L19 16.414V17zm-7 5a2.98 2.98 0 0 0 2.818-2H9.182A2.98 2.98 0 0 0 12 22z"></path>
					</svg>
					<div className="absolute animate-pulse top-[10px] right-[9px] bg-red-500 rounded-full h-[6px] w-[6px]" />
				</div>
			</section>
			<section id="kategori" className="flex overflow-x-auto py-6 scrollbar-hide">
				{kategori.map((k, index) => (
					<div
						className="bg-white rounded-lg shadow-lg px-12 py-2 flex flex-col items-center justify-center mr-6 last:mr-0 min-w-[60px]"
						key={index}>
						<p className="text-3xl">{k.icon}</p>
						<h1 className="text-lg mt-2">{k.nama}</h1>
					</div>
				))}
			</section>
			<section id="search" className="flex w-full rounded-full shadow-md px-6 py-4">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					<path d="M10 18a7.952 7.952 0 0 0 4.897-1.688l4.396 4.396 1.414-1.414-4.396-4.396A7.952 7.952 0 0 0 18 10c0-4.411-3.589-8-8-8s-8 3.589-8 8 3.589 8 8 8zm0-14c3.309 0 6 2.691 6 6s-2.691 6-6 6-6-2.691-6-6 2.691-6 6-6z"></path>
				</svg>
				<input
					type="text"
					className="w-full outline-none pl-4"
					placeholder="Kemana kamu ingin pergi ?"
				/>
			</section>
			<section id="hotel-terpopuler">
				<h1 className="font-semibold text-2xl mt-5">Hotel Terpopuler</h1>
				<div className="flex overflow-x-auto scrollbar-hide py-5">
					{hotels.map((hotel, index) => (
						<div
							className="flex items-end min-w-[240px] h-[220px] relative rounded-2xl overflow-hidden mr-6 last:mr-0 shadow-lg"
							key={index}
							onClick={() => router.push(`/details/${hotel.nama}`)}>
							<Image className="absolute z-[-999]" src={hotel.img} layout="fill" alt={hotel.nama} />
							<div className="bg-white mb-4 mx-auto rounded-md px-4 py-2 w-[90%]">
								<h5 className="font-medium text-xl truncate">{hotel.nama}</h5>
								<div className="flex items-center my-1">
									<svg
										className="mr-1 -ml-2 fill-current text-red-400"
										xmlns="http://www.w3.org/2000/svg"
										width="24"
										height="24"
										viewBox="0 0 20 20">
										<path
											d="M10 2.009c-2.762 0-5 2.229-5 4.99c0 4.774 5 11 5 11s5-6.227 5-11c0-2.76-2.238-4.99-5-4.99zm0 7.751a2.7 2.7 0 1 1 0-5.4a2.7 2.7 0 0 1 0 5.4z"
											fill="currentColor"
										/>
									</svg>
									<p className="truncate opacity-75">{hotel.lokasi}</p>
								</div>
								<div className="flex items-center">
									<svg
										className="mr-1 -ml-2 fill-current text-yellow-400"
										xmlns="http://www.w3.org/2000/svg"
										width="24"
										height="24"
										viewBox="0 0 24 24">
										<path d="M21.947 9.179a1.001 1.001 0 0 0-.868-.676l-5.701-.453-2.467-5.461a.998.998 0 0 0-1.822-.001L8.622 8.05l-5.701.453a1 1 0 0 0-.619 1.713l4.213 4.107-1.49 6.452a1 1 0 0 0 1.53 1.057L12 18.202l5.445 3.63a1.001 1.001 0 0 0 1.517-1.106l-1.829-6.4 4.536-4.082c.297-.268.406-.686.278-1.065z"></path>
									</svg>
									<p className="align-middle text-sm truncate opacity-75">{hotel.review}</p>
								</div>
							</div>
						</div>
					))}
				</div>
			</section>
			<section id="liburan-terpopuler">
				<h1 className="font-semibold text-2xl mb-5">Tempat Liburan Terpopuler</h1>
				<div className="flex flex-col">
					{hotels.map((hotel, index) => (
						<div
							className="flex items-center mb-5"
							key={index}
							onClick={() => router.push(`/details/${hotel.nama}`)}>
							<Image
								className="rounded-2xl shadow"
								src={hotel.img}
								height={100}
								width={140}
								alt={hotel.nama}
							/>
							<div className="ml-5">
								<h1 className="font-medium text-xl">{hotel.nama}</h1>
								<div className="flex items-center my-1">
									<svg
										className="mr-1 -ml-2 fill-current text-red-400"
										xmlns="http://www.w3.org/2000/svg"
										width="24"
										height="24"
										viewBox="0 0 20 20">
										<path
											d="M10 2.009c-2.762 0-5 2.229-5 4.99c0 4.774 5 11 5 11s5-6.227 5-11c0-2.76-2.238-4.99-5-4.99zm0 7.751a2.7 2.7 0 1 1 0-5.4a2.7 2.7 0 0 1 0 5.4z"
											fill="currentColor"
										/>
									</svg>
									<p className="truncate opacity-75">{hotel.lokasi}</p>
								</div>
								<div className="flex items-center">
									<svg
										className="mr-1 -ml-2 fill-current text-yellow-400"
										xmlns="http://www.w3.org/2000/svg"
										width="24"
										height="24"
										viewBox="0 0 24 24">
										<path d="M21.947 9.179a1.001 1.001 0 0 0-.868-.676l-5.701-.453-2.467-5.461a.998.998 0 0 0-1.822-.001L8.622 8.05l-5.701.453a1 1 0 0 0-.619 1.713l4.213 4.107-1.49 6.452a1 1 0 0 0 1.53 1.057L12 18.202l5.445 3.63a1.001 1.001 0 0 0 1.517-1.106l-1.829-6.4 4.536-4.082c.297-.268.406-.686.278-1.065z"></path>
									</svg>
									<p className="align-middle text-sm truncate opacity-75">{hotel.review}</p>
								</div>
							</div>
						</div>
					))}
				</div>
			</section>
		</>
	)
}

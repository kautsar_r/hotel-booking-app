import { useRouter } from 'next/router'
import Image from 'next/image'

// Import data
import hotels from '../../data/populer-hotel'
import { useState } from 'react'

export default function detail({ hotel }) {
	const router = useRouter()
	const [more, setMore] = useState(false)

	const h = hotel.filter((h) => h.nama == router.query.nama)[0]

	const desc = h.deskirpsi.substring(0, 80)

	return (
		<>
			<section id="banner" className="relative w-full mb-4">
				<Image
					src={h.img}
					className="absolute w-full rounded-3xl"
					alt="Banner"
					width={400}
					height={400}
				/>
				<button
					className="absolute left-2 top-2 rounded-full p-2 bg-white shadow"
					onClick={() => router.push('/')}>
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
						<path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z"></path>
					</svg>
				</button>
			</section>
			<section id="detail">
				<h1 className="font-semibold text-3xl mb-2">{h.nama}</h1>
				<div className="flex items-center my-1">
					<svg
						className="mr-1 -ml-2 fill-current text-red-400"
						xmlns="http://www.w3.org/2000/svg"
						width="24"
						height="24"
						viewBox="0 0 20 20">
						<path
							d="M10 2.009c-2.762 0-5 2.229-5 4.99c0 4.774 5 11 5 11s5-6.227 5-11c0-2.76-2.238-4.99-5-4.99zm0 7.751a2.7 2.7 0 1 1 0-5.4a2.7 2.7 0 0 1 0 5.4z"
							fill="currentColor"
						/>
					</svg>
					<p className="truncate opacity-75">{h.lokasi}</p>
				</div>
				<div className="flex items-center">
					<svg
						className="mr-1 -ml-2 fill-current text-yellow-400"
						xmlns="http://www.w3.org/2000/svg"
						width="24"
						height="24"
						viewBox="0 0 24 24">
						<path d="M21.947 9.179a1.001 1.001 0 0 0-.868-.676l-5.701-.453-2.467-5.461a.998.998 0 0 0-1.822-.001L8.622 8.05l-5.701.453a1 1 0 0 0-.619 1.713l4.213 4.107-1.49 6.452a1 1 0 0 0 1.53 1.057L12 18.202l5.445 3.63a1.001 1.001 0 0 0 1.517-1.106l-1.829-6.4 4.536-4.082c.297-.268.406-.686.278-1.065z"></path>
					</svg>
					<p className="align-middle text-sm truncate opacity-75">{h.review}</p>
				</div>
				<div className="my-3">
					{more ? h.deskirpsi : desc}
					<span className="text-blue-500" onClick={() => setMore(!more)}>
						{more ? '...Readless' : '...Readmore'}
					</span>
				</div>
				<div class="flex justify-between mb-[84px] w-full">
					{h.service.map((s, index) => (
						<div class="flex flex-col items-center" key={index}>
							<p className="text-2xl mb-3">{s.icon}</p>
							<p>{s.name}</p>
						</div>
					))}
				</div>
			</section>
			<section
				id="book"
				className="fixed flex justify-between items-center left-0 right-0 bottom-0 px-4 h-[110px] bg-white">
				<h1 className="font-semibold text-2xl">Rp. {h.harga}</h1>
				<button className="px-10 py-3 bg-blue-500 text-white text-lg rounded-full shadow">
					Book Now
				</button>
			</section>
		</>
	)
}

export async function getStaticPaths() {
	const paths = hotels.map((hotel) => {
		return {
			params: {
				nama: hotel.nama
			}
		}
	})

	return { paths, fallback: false }
}

export function getStaticProps() {
	return {
		props: {
			hotel: hotels
		}
	}
}

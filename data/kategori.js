const kategori = [
  { nama: 'Island', icon: '🏝️' },
  { nama: 'Hotel', icon: '🏨' },
  { nama: 'Train', icon: '🚄' },
  { nama: 'Flights', icon: '✈️' },
  { nama: 'Camping', icon: '🏕️' },
]

module.exports = kategori

const hotels = [
	{
		nama: 'Hotel seruni',
		lokasi: 'Puncak, Bogor',
		review: '4.8/5 (1003)',
		img: '/img/hotel1.jpg',
		harga: '200.000',
		deskirpsi:
			'Akomodasi ini bersih, aman, dan tersertifikasi CHSE dari Kementerian Pariwisata dan Ekonomi Kreatif. Berlokasi strategis di pusat Kuta Bali, hanya 20 menit berjalan kaki ke Pantai Kuta yang terkenal, 2 menit ke Pantai Segara yang nyaman, 5 menit berkendara dari Bandara lokasi "Gusti Ngurah Rai International Airport" dan 10 menit berjalan kaki ke Jalan Kartika Plaza yang terkenal sebagai lokasi pariwisata, Pusat perbelanjaan, serta pusat jajanan minuman dan makanan.',
		service: [
			{ name: 'Family', icon: '👨‍👩‍👧‍👦' },
			{ name: 'Launch', icon: '🍽' },
			{ name: 'Dessert', icon: '🍰' },
			{ name: 'WI-FI', icon: '📶' },
			{ name: 'Pets', icon: '🐈' }
		]
	},
	{
		nama: 'Hotel jayabaya',
		lokasi: 'Cisarua, Bogor',
		review: '4.8/5 (993)',
		img: '/img/hotel2.jpg',
		harga: '200.000',
		deskirpsi:
			'Akomodasi ini bersih, aman, dan tersertifikasi CHSE dari Kementerian Pariwisata dan Ekonomi Kreatif. Berlokasi strategis di pusat Kuta Bali, hanya 20 menit berjalan kaki ke Pantai Kuta yang terkenal, 2 menit ke Pantai Segara yang nyaman, 5 menit berkendara dari Bandara lokasi "Gusti Ngurah Rai International Airport" dan 10 menit berjalan kaki ke Jalan Kartika Plaza yang terkenal sebagai lokasi pariwisata, Pusat perbelanjaan, serta pusat jajanan minuman dan makanan.',
		service: [
			{ name: 'Family', icon: '👨‍👩‍👧‍👦' },
			{ name: 'Launch', icon: '🍽' },
			{ name: 'Dessert', icon: '🍰' },
			{ name: 'WI-FI', icon: '📶' },
			{ name: 'Pets', icon: '🐈' }
		]
	},
	{
		nama: 'Hotel nusa indah',
		lokasi: 'Lembang, Bogor',
		review: '4.8/5 (886)',
		img: '/img/hotel3.jpg',
		harga: '200.000',
		deskirpsi:
			'Akomodasi ini bersih, aman, dan tersertifikasi CHSE dari Kementerian Pariwisata dan Ekonomi Kreatif. Berlokasi strategis di pusat Kuta Bali, hanya 20 menit berjalan kaki ke Pantai Kuta yang terkenal, 2 menit ke Pantai Segara yang nyaman, 5 menit berkendara dari Bandara lokasi "Gusti Ngurah Rai International Airport" dan 10 menit berjalan kaki ke Jalan Kartika Plaza yang terkenal sebagai lokasi pariwisata, Pusat perbelanjaan, serta pusat jajanan minuman dan makanan.',
		service: [
			{ name: 'Family', icon: '👨‍👩‍👧‍👦' },
			{ name: 'Launch', icon: '🍽' },
			{ name: 'Dessert', icon: '🍰' },
			{ name: 'WI-FI', icon: '📶' },
			{ name: 'Pets', icon: '🐈' }
		]
	},
	{
		nama: 'Hotel indah kapuk',
		lokasi: 'Jawa, Bogor',
		review: '4.8/5 (694)',
		img: '/img/hotel4.jpg',
		harga: '200.000',
		deskirpsi:
			'Akomodasi ini bersih, aman, dan tersertifikasi CHSE dari Kementerian Pariwisata dan Ekonomi Kreatif. Berlokasi strategis di pusat Kuta Bali, hanya 20 menit berjalan kaki ke Pantai Kuta yang terkenal, 2 menit ke Pantai Segara yang nyaman, 5 menit berkendara dari Bandara lokasi "Gusti Ngurah Rai International Airport" dan 10 menit berjalan kaki ke Jalan Kartika Plaza yang terkenal sebagai lokasi pariwisata, Pusat perbelanjaan, serta pusat jajanan minuman dan makanan.',
		service: [
			{ name: 'Family', icon: '👨‍👩‍👧‍👦' },
			{ name: 'Launch', icon: '🍽' },
			{ name: 'Dessert', icon: '🍰' },
			{ name: 'WI-FI', icon: '📶' },
			{ name: 'Pets', icon: '🐈' }
		]
	}
]

module.exports = hotels
